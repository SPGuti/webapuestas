package mvc.controlador;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mvc.controlador.acciones.DesconectarAction;
import mvc.controlador.acciones.RealizarApuestaAction;
import mvc.controlador.acciones.RecargarAction;
import mvc.controlador.acciones.RegistrarAction;
import mvc.controlador.acciones.ValidarAction;

@WebServlet(name = "ActionServlet", urlPatterns = {"/ActionServlet"})
public class ActionServlet extends HttpServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String vista = "/index.jsp";

        String accion = req.getParameter("act");

        switch (accion) {
            case "val":
                // Validar formulario entrada
                ValidarAction action = new ValidarAction();

                vista = action.execute(req);

                break;
            case "reg":
                // Registrar datos usuario 
                RegistrarAction action2 = new RegistrarAction();

                vista = action2.execute(req);

                break;
            case "des":
                DesconectarAction action3 = new DesconectarAction();

                vista = action3.execute(req);

                break;
            case "rec":
                RecargarAction action4 = new RecargarAction();

                vista = action4.execute(req);

                break;
                
            case "realizarApu":
                // Ver datos del usuario 
                
                RealizarApuestaAction action5 = new RealizarApuestaAction();

        {
            try {
                vista = action5.execute(req);
            } catch (Exception ex) {
                Logger.getLogger(ActionServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

                break;

        }
        String pagina = resp.encodeURL(vista);

        RequestDispatcher dispatcher = req.getRequestDispatcher(pagina);

        dispatcher.forward(req, resp);
    }
}
