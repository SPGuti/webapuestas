package mvc.controlador.acciones;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import mvc.modelo.dao.UsuarioJpaController;
import mvc.modelo.entidades.Usuario;

public class ValidarAction {
    
    public String execute(HttpServletRequest request) throws ServletException {

        // Recuperar parametros de peticion
        String usuario = request.getParameter("usuario");
        String contrasenia = request.getParameter("contrasenia");

        // Validar datos
        // Nombre y correo OBLIGATORIOS
        if (usuario.isEmpty() || contrasenia.isEmpty()) {
            // Crear atributo con el error de la validacion
            request.setAttribute("error", "Usuario y contraseña son obligatorios");

            
            return "/index.jsp";
        }
        
        UsuarioJpaController controller = new UsuarioJpaController();
        
        EntityManager em = controller.getEntityManager();
        
        Query q = em.createNamedQuery("Usuario.find");
        
        q.setParameter("usuario", usuario);
        q.setParameter("contrasenia", contrasenia);
        
        Usuario u = null;
        
        try {
            u = (Usuario) q.getSingleResult();
            // Recuperar la sesion (usuario) actual
            HttpSession session = request.getSession();
        
            session.setAttribute("usuario", u);
        
            return "/inicial.jsp";
        }catch(Exception ex) {
            request.setAttribute("error", "Usuario o contraseña incorrecta");
            
            return "/index.jsp";
        } finally {
            em.close();
        }

        
    }
    
}
