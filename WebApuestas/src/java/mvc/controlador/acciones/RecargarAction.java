package mvc.controlador.acciones;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import mvc.modelo.dao.UsuarioJpaController;
import mvc.modelo.entidades.Usuario;

public class RecargarAction {
    
    public String execute(HttpServletRequest request) throws ServletException {
        
        if(request.getParameter("saldoadicional").isEmpty()) {
            request.setAttribute("errorRegistro", "Saldo vacío");

            return "/perfil.jsp";
        }

        Double saldoadicional = Double.parseDouble(request.getParameter("saldoadicional"));
        
        UsuarioJpaController controller = new UsuarioJpaController();

        /*EntityManager em = controller.getEntityManager();
        
        Query q = em.createNamedQuery("Usuario.find");*/

        Usuario u = (Usuario) request.getSession().getAttribute("usuario");
        
        try{
            
            //u = (Usuario) q.getSingleResult();
            
            Double saldoexistente = u.getSaldo();
            
            Double suma = saldoadicional + saldoexistente;
            
            u.setSaldo(suma);
            
            controller.edit(u);

            //session.setAttribute("usuario", usuario1);

            return "/inicial.jsp";
            
        }catch(Exception ex){
            
            request.setAttribute("errorRegistro", "ERROR");

            return "/perfil.jsp";
            
        
        }
    }
    
}
