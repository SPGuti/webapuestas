
package mvc.controlador.acciones;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import mvc.modelo.dao.UsuarioApuestaJpaController;
import mvc.modelo.dao.UsuarioJpaController;
import mvc.modelo.entidades.Apuesta;
import mvc.modelo.entidades.Usuario;
import mvc.modelo.entidades.UsuarioApuesta;
import mvc.modelo.entidades.UsuarioApuestaPK;


public class RealizarApuestaAction {
    
    
    public String execute(HttpServletRequest request) throws Exception {
        
        

        double apostado = Double.parseDouble(request.getParameter("apostado"));

        HttpSession session = request.getSession(false);
        Usuario usu = ((Usuario) session.getAttribute("usuario"));
        int idUsu = usu.getIdUsuario();
   
        int idApu = (int) session.getAttribute("idApuesta");

            UsuarioApuestaJpaController apuController = new UsuarioApuestaJpaController();
            UsuarioApuestaPK apuestaPK = new UsuarioApuestaPK(idUsu, idApu);
            UsuarioApuesta ua = new UsuarioApuesta(apuestaPK, apostado);
            ua.setApuesta(apuController.getEntityManager().find(Apuesta.class, idApu));
            apuController.create(ua);

            UsuarioJpaController usuController = new UsuarioJpaController();

            usu.setSaldo(usu.getSaldo() - apostado);
            usuController.edit(usu);

            return "/apuestaRealizada.jsp";
        
    }
    
}
