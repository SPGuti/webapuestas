/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mvc.modelo.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Grupo 1 Java
 */
@Entity
@Table(name = "apuesta")
@NamedQueries({
    @NamedQuery(name = "Apuesta.findAll", query = "SELECT a FROM Apuesta a")
    , @NamedQuery(name = "Apuesta.findByIdApuesta", query = "SELECT a FROM Apuesta a WHERE a.idApuesta = :idApuesta")
    , @NamedQuery(name = "Apuesta.findByDeporte", query = "SELECT a FROM Apuesta a WHERE a.deporte = :deporte")
    , @NamedQuery(name = "Apuesta.findByFechaPartido", query = "SELECT a FROM Apuesta a WHERE a.fechaPartido = :fechaPartido")
    , @NamedQuery(name = "Apuesta.findByLocal", query = "SELECT a FROM Apuesta a WHERE a.local = :local")
    , @NamedQuery(name = "Apuesta.findByVisitante", query = "SELECT a FROM Apuesta a WHERE a.visitante = :visitante")
    , @NamedQuery(name = "Apuesta.findByMercado", query = "SELECT a FROM Apuesta a WHERE a.mercado = :mercado")
    , @NamedQuery(name = "Apuesta.findByCuota", query = "SELECT a FROM Apuesta a WHERE a.cuota = :cuota")
    , @NamedQuery(name = "Apuesta.findByAcertada", query = "SELECT a FROM Apuesta a WHERE a.acertada = :acertada")})
public class Apuesta implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_apuesta")
    private Integer idApuesta;
    @Column(name = "deporte")
    private String deporte;
    @Column(name = "fecha_partido")
    private String fechaPartido;
    @Column(name = "local")
    private String local;
    @Column(name = "visitante")
    private String visitante;
    @Column(name = "mercado")
    private String mercado;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "cuota")
    private Double cuota;
    @Column(name = "acertada")
    private Integer acertada;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "apuesta")
    private List<UsuarioApuesta> usuarioApuestaList;

    public Apuesta() {
    }

    public Apuesta(Integer idApuesta) {
        this.idApuesta = idApuesta;
    }

    public Integer getIdApuesta() {
        return idApuesta;
    }

    public void setIdApuesta(Integer idApuesta) {
        this.idApuesta = idApuesta;
    }

    public String getDeporte() {
        return deporte;
    }

    public void setDeporte(String deporte) {
        this.deporte = deporte;
    }

    public String getFechaPartido() {
        return fechaPartido;
    }

    public void setFechaPartido(String fechaPartido) {
        this.fechaPartido = fechaPartido;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public String getVisitante() {
        return visitante;
    }

    public void setVisitante(String visitante) {
        this.visitante = visitante;
    }

    public String getMercado() {
        return mercado;
    }

    public void setMercado(String mercado) {
        this.mercado = mercado;
    }

    public Double getCuota() {
        return cuota;
    }

    public void setCuota(Double cuota) {
        this.cuota = cuota;
    }

    public Integer getAcertada() {
        return acertada;
    }

    public void setAcertada(Integer acertada) {
        this.acertada = acertada;
    }

    public List<UsuarioApuesta> getUsuarioApuestaList() {
        return usuarioApuestaList;
    }

    public void setUsuarioApuestaList(List<UsuarioApuesta> usuarioApuestaList) {
        this.usuarioApuestaList = usuarioApuestaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idApuesta != null ? idApuesta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Apuesta)) {
            return false;
        }
        Apuesta other = (Apuesta) object;
        if ((this.idApuesta == null && other.idApuesta != null) || (this.idApuesta != null && !this.idApuesta.equals(other.idApuesta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mvc.modelo.entidades.Apuesta[ idApuesta=" + idApuesta + " ]";
    }
    
}
