<%-- 
    Document   : realizarApuesta
    Created on : 09-abr-2018, 12:54:50
    Author     : Grupo 1 Java
--%>

<%@page import="mvc.modelo.entidades.Usuario"%>
<%@page import="mvc.modelo.entidades.Apuesta"%>
<%@page import="javax.persistence.Query"%>
<%@page import="javax.persistence.EntityManager"%>
<%@page import="javax.persistence.Persistence"%>
<%@page import="javax.persistence.EntityManagerFactory"%>
<%@page import="javax.persistence.EntityManagerFactory"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>realizarApuesta</title>
    </head>
    <body>
        <%
            int id = Integer.parseInt(request.getParameter("apuesta"));
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("PU");
            EntityManager em = emf.createEntityManager();
            Query q = em.createNamedQuery("Apuesta.findByIdApuesta");
            q.setParameter("idApuesta", id);
            Apuesta apu = (Apuesta) q.getSingleResult();
            session.setAttribute("idApuesta", apu.getIdApuesta());
            
        %>
        <h1><%= apu.getLocal() %> - <%= apu.getVisitante() %></h1>
        <h2><%= apu.getFechaPartido()%></h2>
        <h2>Pronóstico: <%= apu.getMercado()%></h2>
        <h2>Cuota: <%= apu.getCuota()%></h2>
        
        <form method="post" action='<%= response.encodeURL("ActionServlet?act=realizarApu")%>'>
            <label for="nombre"> ¿Cuanto dinero desea apostar? </label>
            <input type="number" name="apostado" id="apostado" size="10" maxlength="50" max="<%= ((Usuario) session.getAttribute("usuario")).getSaldo()%>"/>
            <input type="submit" value="Apostar"/>
        </form>

        
    </body>
</html>
