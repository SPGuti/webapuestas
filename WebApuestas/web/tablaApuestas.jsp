<%@page import="java.util.HashSet"%>
<%@page import="java.util.Set"%>
<%@page import="javax.persistence.Query"%>
<%@page import="javax.persistence.EntityManager"%>
<%@page import="javax.persistence.Persistence"%>
<%@page import="javax.persistence.EntityManagerFactory"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="com.mysql.jdbc.PreparedStatement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="com.mysql.jdbc.Connection"%>
<%@page import="mvc.modelo.entidades.Apuesta"%>
<%@page import="java.util.List"%>
<%@page import="mvc.modelo.entidades.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="home.css">
        <title>Todas las Apuestas</title>
    </head>
    <body>

        <section style="width: 100%; margin-left: 350px; margin-top: 150px">


            <%

                EntityManagerFactory emf = Persistence.createEntityManagerFactory("PU");
                EntityManager em = emf.createEntityManager();

                Query q = em.createNamedQuery("Apuesta.findAll");
                List<Apuesta> lApuestas = q.getResultList();

                //DESPLEGABLE
                /*
                Set<String> hsApuesta = new HashSet<>();
                for (Apuesta apu : lApuestas) {
                    hsApuesta.add(apu.getDeporte().toLowerCase());
                }
                out.print("<select name=\"deportes\">"
                        + "<option selected value='todas'>TODAS</option>");
                for (String deporte : hsApuesta) {
                        out.println("<option value='" + deporte.toLowerCase() + "'>"
                                + deporte.toUpperCase() + "</option>");
                    }
                out.print("</select>");
                */
                //TABLA
                out.print("<table id='tabla' style='border: 1px solid black;'>");
                out.print("<tr>");
                    out.println("<th>DEPORTE</th><th>FECHA</th><th>EQUIPO LOCAL</th><th>EQUIPO VISITANTE</th><th>PRONÓSTICO</th><th>CUOTA</th><th>APUESTA</th>");
                    out.print("</tr>");
                for (Apuesta apu : lApuestas) {
                    out.print("<tr>");
                    out.println("<td>" + apu.getDeporte().toUpperCase() + "</td><td>" + apu.getFechaPartido() + "</td><td>"
                            + apu.getLocal().toUpperCase()  + "</td><td>" + apu.getVisitante().toUpperCase()  + "</td><td>"
                            + apu.getMercado().toUpperCase()  + "</td><td>" + apu.getCuota()
                            + "</td><td><a href='" + "realizarApuesta.jsp?apuesta=" + apu.getIdApuesta() + "'>Apostar</a></td>");
                    out.print("</tr>");
                }
                out.print("</table>");
            %>


            <%-- <body>
                    <form method="get" action='<%=response.encodeURL("apuestas")%>'>
                        <strong>Deportes</strong>
                        &nbsp;
                        <select name="deportes">

                    /*
                    EntityManagerFactory emf = Persistence.createEntityManagerFactory("PU");
                    EntityManager em = emf.createEntityManager();
                    
                    Query q = em.createNamedQuery("Apuesta.findAll");
                    List<Apuesta> deportes = q.getResultList();
                    for (Apuesta apu : deportes) {
                        out.println("<option value='" + apu.getDeporte().toLowerCase() + "'>"
                                + apu.getDeporte() + "</option>");
                    }*/

            </select>
            <br/><br/>
            <input type="submit" value="   CONSULTAR   " />
        </form>
    </body> --%>

        </section>
    </body>



</html>
