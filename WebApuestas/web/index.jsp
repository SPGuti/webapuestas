<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>LOGIN</title>
        <link rel="stylesheet" href="style.css">
        <link rel="stylesheet" type="text/css" href="home.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" />
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
        
    </head>
    <body>

        <jsp:useBean id="formu" class="mvc.modelo.entidades.Datos"
                     scope="request" />
        <jsp:setProperty name="formu" property="*" />
        
        <div class="cabecera-bloque">
        <div class="izquierda-bloque">
                    <div class="logotexto">BIENVENIDO a Apuestas.com </div> 
                    
                   
        </div>
        </div>

<div  class="cabecera-bloque1">    
  <div class="contenedor">
      <div>
     <form  id="fmrlogin" method="post" action='<%= response.encodeURL("ActionServlet?act=val")%>'>
            
            <div class="container" style=" margin: 90px auto 240px 490px; 
                 padding: 20px 20px 20px; width: 350px;  background: white; border-radius: 3px;">

                <div class="page-header" >
                    <h1 style="font-size: xx-large">Entrar o Registrarse</h1>
                    
                    <br style="size: auto">
                 
          <div class=" alert-danger"><%
            if (request.getAttribute("error") != null) {
                out.println( request.getAttribute("error"));}%></div>
        
              <br style="size: auto">      
                </div>
                <div class="form-group">        
            <label for="usuario">Usuario: </label>
            <input type="text" name="usuario" id="usuario"
                   maxlength="10" required placeholder="Introduce tu usuario."
                   value="<jsp:getProperty name="formu" property="usuario" />" />
                </div>
                
                <div class="form-group">
            <label for="contrasenia">Contraseña: </label>
            <input type="password" name="contrasenia" id="contrasenia"
                   maxlength="15" required placeholder="Contraseña."
                   value="<jsp:getProperty name="formu" property="contrasenia" />" />
                </div>
                <input id="btnEntrar" type="submit" class="btn btn-primary" value=" ENTRAR "/>
                
            
                <a href='registro.jsp'><input  id="btnRegistrarse"type="button" class="btn btn-primary" value="REGISTRARSE" /></a>
            
            </div>
            
</form>
      
        </div>
              
                
       
  </div>
                   <%--
            <div class="cabecera-bloque-reg">
          <p class="logotexto">
              </p>
      </div>
            
                   --%>
    </body>
</html>
