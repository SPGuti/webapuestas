<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>LOGIN</title>
        <link rel="stylesheet" href="style.css">
        <link rel="stylesheet" type="text/css" href="home.css">
    </head>
    <body>
        <%--
            Comprobar si existe atributo error en la peticionasd
        --%>
        <%
            if (request.getAttribute("error") != null) {
                // Existe. Mostrar con texto en rojo
                out.println("<font color='red'>");
                out.println("<h4>" + request.getAttribute("error") + "</h4>");
                out.println("</font>");
            }
        %>

        <jsp:useBean id="formu" class="mvc.modelo.entidades.Datos"
                     scope="request" />

        <jsp:setProperty name="formu" property="*" />
        
        <div class="cabecera-bloque">
        <div class="izquierda-bloque">
                    <div class="logotexto">BIENVENIDO a Apuestas.com </div> 
                    
                    <span class="subtitulo">Tu Web de apuestas deportivas.</span>
        </div>
        </div>

<div  class="cabecera-bloque1">    
  <div class="contenedor">
    <div class="login">
      <h1>Entrar o Registrarse</h1>
      
        <form method="post" action='<%= response.encodeURL("ActionServlet?act=val")%>'>
            <label for="usuario">Usuario: </label>
            <input type="text" name="usuario" id="usuario"
                   size="10" maxlength="10" required
                   value="<jsp:getProperty name="formu" property="usuario" />" />
            <br/><br/>
            <label for="contrasenia">Contraseña: </label>
            <input type="password" name="contrasenia" id="contrasenia"
                   size="15" maxlength="15" required
                   value="<jsp:getProperty name="formu" property="contrasenia" />" />
            <br/><br/>
            <input type="submit" value=" ENTRAR "/>
            
            <a href='registro.jsp'><input type="button" value="REGISTRARSE" /></a>
            
</form>
      
        </div>
       
  </div>
            
            <div class="cabecera-bloque-reg">
          <p class="logotexto">
              </p>
      </div>
            >
             
    </body>
</html>
