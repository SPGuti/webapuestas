
<%@page import="java.util.ArrayList"%>
<%@page import="mvc.modelo.entidades.UsuarioApuestaPK"%>
<%@page import="mvc.modelo.entidades.UsuarioApuesta"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.HashSet"%>
<%@page import="java.util.List"%>
<%@page import="javax.persistence.Persistence"%>
<%@page import="javax.persistence.Query"%>
<%@page import="javax.persistence.EntityManager"%>
<%@page import="javax.persistence.EntityManagerFactory"%>
<%@page import="mvc.modelo.entidades.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@page import="mvc.modelo.entidades.Apuesta"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="style.css">
        <link rel="stylesheet" type="text/css" href="home.css">
        <title>PERFIL</title>
    </head>
    <body>

        <jsp:include page="encabezado.jsp" flush="true" />

        <div>
            <div>
                <div >
                    <%
                        // Recuperar atributo de sesion usuario
                        Usuario u = (Usuario) session.getAttribute("usuario");

                        out.print("<h1>" + u.getAlias() + "</h1>");
                        out.print("<h3>" + u.getSaldo() + "</h3>");

                        EntityManagerFactory emf = Persistence.createEntityManagerFactory("PU");
                        EntityManager em = emf.createEntityManager();
                        Query q = em.createNamedQuery("UsuarioApuesta.findByIdUsuario");
                        q.setParameter("idUsuario", u.getIdUsuario());

                        List<UsuarioApuesta> listUsuApu = q.getResultList();

                        out.print("apuestas realizadas: " + listUsuApu.size() + "<br/>");
                        //out.print("<br/>elemento " + listUsuApu.get(1)); 
                        //out.print("<br/>elementos " + listUsuApu); 
                        //List<Apuesta> lApuestas = new ArrayList<Apuesta>();

                        //TABLA
                        out.print("<table id='tabla' style='border: 1px solid black;'>");
                        out.println("<th>DEPORTE</th><th>FECHA</th><th>EQUIPO LOCAL</th><th>EQUIPO VISITANTE</th><th>PRONÓSTICO</th><th>CUOTA</th><th>APOSTADO</th>");
                        for (UsuarioApuesta ua : listUsuApu) {
                            out.print("<tr>");
                            out.println("<td>" + ua.getApuesta().getDeporte().toUpperCase() + "</td><td>" + ua.getApuesta().getFechaPartido() + "</td><td>"
                                    + ua.getApuesta().getLocal().toUpperCase() + "</td><td>" + ua.getApuesta().getVisitante().toUpperCase() + "</td><td>"
                                    + ua.getApuesta().getMercado().toUpperCase() + "</td><td>" + ua.getApuesta().getCuota() + "</td><td>" + ua.getApostado() + "€</td>");
                            out.print("</tr>");

                        }

                        out.print("</table>");

                    %>

                    <form method="post" action='<%= response.encodeURL("ActionServlet?act=rec")%>'>
                        <label for="saldoadicional">Recargar saldo: </label>
                        <input type="number" name="saldoadicional" id="saldoadicional"
                               size="6" maxlength="6" min="0.0" max="500.0" step="0.1" />

                        <br/><br/>
                        <input type="submit" value="   RECARGAR   " />
                    </form>


                </div>
            </div>
        </div>
    </body>
</html>
