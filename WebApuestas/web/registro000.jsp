<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>REGISTRO</title>
         <link rel="stylesheet" href="style.css">
        <link rel="stylesheet" type="text/css" href="home.css">
    </head>
    <body>
        
        
        <%
            if (request.getAttribute("errorRegistro") != null) {
                // Existe. Mostrar con texto en rojo
                out.println("<font color='red'>");
                out.println("<h4>" + request.getAttribute("errorRegistro") + "</h4>");
                out.println("</font>");
            }
        %>
        
        <jsp:useBean id="formu" class="mvc.modelo.entidades.Datos"
                     scope="request" />
        
        <jsp:setProperty name="formu" property="*" />
        
        <div class="cabecera-bloque">
        <div class="izquierda-bloque">
                    <div class="logotexto">BIENVENIDO a Apuestas.com </div> 
                    
                    <span class="subtitulo">Tu Web de apuestas deportivas.</span>
        </div>
        </div>
        
       <div  class="cabecera-bloque1">   
      <div class="contenedor">
          <div class="login">
  
        <form method="post" action='<%= response.encodeURL("ActionServlet?act=reg")%>'>
            <h3>CREAR NUEVO USUARIO</h3>
            <label for="usuario">Usuario: </label>
            <input type="text" name="usuario" id="usuario"
                   size="10" maxlength="10" required
                   value="<jsp:getProperty name="formu" property="usuario" />" />
            <br/><br/>
            <label for="contrasenia">Contraseña: </label>
            <input type="password" name="contrasenia" id="contrasenia"
                   size="15" maxlength="15" required
                   value="<jsp:getProperty name="formu" property="contrasenia" />" />
            <br/><br/>
            <label for="repcontrasenia">Repetir contraseña: </label>
            <input type="password" name="repcontrasenia" id="repcontrasenia"
                   size="15" maxlength="15" required
                   value="<jsp:getProperty name="formu" property="repcontrasenia" />" />
            <br/><br/>
            <label for="saldo">Saldo inicial: </label>
            <input type="text" name="saldo" id="saldo"
                   size="6" maxlength="6" 
                   value="<jsp:getProperty name="formu" property="saldo" />" />
            <br/><br/>
            <input type="submit" value="   FINALIZAR REGISTRO   " />
        </form>
            
            </div>
            </div>
      
      <div class="cabecera-bloque-reg">
          <p class="logotexto">
              </p>
      </div>
    </body>
</html>
