<%-- 
    Document   : navegacion
    Created on : 10-abr-2018, 11:25:07
    Author     : Grupo 1 Java
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
       <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Barra Navegacion Bootstrap</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" />
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>

        <style type="text/css">
            .sample {
                font-size: 130%;
                border: 1px dotted silver;
                padding: 15px;
            }
        </style>
    </head>
    <body style="margin: 50px;">
        <div class="sample container">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link" href="inicial.jsp">Inicio</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="contacto.jsp">Contacto</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="https://www.ordenacionjuego.es/es/juego-responsable-dgoj">Juego Resposable</a>
                </li>
                
        </div>
    </body>
</html>
