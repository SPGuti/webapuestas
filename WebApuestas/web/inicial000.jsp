

<%@page import="mvc.modelo.entidades.Usuario"%>
<%@page import="javax.persistence.Query"%>
<%@page import="javax.persistence.EntityManager"%>
<%@page import="javax.persistence.Persistence"%>
<%@page import="javax.persistence.EntityManagerFactory"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="style.css">
        <link rel="stylesheet" type="text/css" href="home.css">
        <title>INICIAL</title>
    </head>
    
    <%
        Usuario u = (Usuario) session.getAttribute("usuario");
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("PU");
        EntityManager em = emf.createEntityManager();
        Query q = em.createNamedQuery("Usuario.findByAlias");
        q.setParameter("alias", u.getAlias());
        
        session.setAttribute("saldo", q.getFirstResult());
    %>
    
        <header>
            <jsp:include page="encabezado.jsp" flush="true" />
        </header>
   
        <section>
            <jsp:include page="tablaApuestas.jsp" flush="true" />
        </section>>
        
     
        <footer>
            <jsp:include page="pie.jsp" flush="true" />
        </footer>
    
</html>
