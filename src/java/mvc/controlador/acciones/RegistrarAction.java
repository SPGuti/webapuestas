package mvc.controlador.acciones;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import mvc.modelo.dao.UsuarioJpaController;
import mvc.modelo.entidades.Usuario;

public class RegistrarAction {

    public String execute(HttpServletRequest request) throws ServletException {

        // Recuperar parametros de peticion
        String usuario = request.getParameter("usuario");
        String contrasenia = request.getParameter("contrasenia");
        String repcontrasenia = request.getParameter("repcontrasenia");
        //String alias = request.getParameter("alias");

        // Validar datos
        // Usuario y contraseña son OBLIGATORIOS
        if (usuario.isEmpty() || contrasenia.isEmpty() || repcontrasenia.isEmpty()) {
            // Crear atributo con el error de la validacion
            request.setAttribute("errorRegistro", "Usuario y contraseñas son obligatorios");

            return "/registro.jsp";

        } else if (!contrasenia.equals(repcontrasenia)) {
            request.setAttribute("errorRegistro", "Contraseñas deben ser idénticas");

            return "/registro.jsp";

        } else if (request.getParameter("saldo").isEmpty()) {
            request.setAttribute("errorRegistro", "Saldo vacío, si no desea saldo inicial introduzca 0");

            return "/registro.jsp";
        }

        Double saldo = Double.parseDouble(request.getParameter("saldo"));

        UsuarioJpaController controller = new UsuarioJpaController();

        EntityManager em = controller.getEntityManager();

        //Query q = em.createNamedQuery("Usuario.findByAlias");

        //q.setParameter("alias", alias);

       

        try {

            //if (!usuario.equals(alias)) {

                Usuario usuario1 = new Usuario(saldo, usuario, repcontrasenia, 0);

                controller.create(usuario1);

                HttpSession session = request.getSession();

                session.setAttribute("usuario", usuario1);

                return "/inicial.jsp";

           // }

        } catch (Exception ex) {

            request.setAttribute("errorRegistro", "Usuario ya existente, escoja otro");

            return "/registro.jsp";

        } finally {

            em.close();

        }

    }

}
