package mvc.controlador.acciones;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class DesconectarAction {
    
    public String execute(HttpServletRequest request) {
        
        // Recuperar la sesion SIN CREARLA en caso de no existir
        HttpSession session = request.getSession(false);
        
        // Comprobar si existia
        if(session != null) {
            // Finalizar
            session.invalidate();
            
            request.setAttribute("error", "Has finalizado tu sesión");
        } else {
            request.setAttribute("error", "Tu sesión ha expirado");
        }
        
        return "/index.jsp";
    }
    
}
