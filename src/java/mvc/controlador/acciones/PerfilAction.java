
package mvc.controlador.acciones;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


public class PerfilAction {
    
    
    public String route(HttpServletRequest request) {

        // Recuperar sesion sin crearla
        HttpSession session = request.getSession(false);

        // Comprobar si el usuario está validado
        if (session != null && session.getAttribute("usuario") != null) {
            return "/perfil.jsp";
        }

        request.setAttribute("error", "Tu sesión ha expirado");
        
        return "/index.jsp";
    }
    
}
