package mvc.modelo.entidades;

public class Datos {
    
    private String usuario="";
    private String contrasenia="";
    private String repcontrasenia="";
    private String saldo="";

    public Datos() {
        //Necesario para utilizar Java Bean
    }

    public Datos(String usuario, String contrasenia) {
        this.usuario = usuario;
        this.contrasenia = contrasenia;
    }
    
    public Datos(String usuario, String contrasenia, String repcontrasenia, String saldo){
        this.usuario = usuario;
        this.contrasenia = contrasenia;
        this.repcontrasenia = repcontrasenia;
        this.saldo = saldo;
    }

    public String getUsuario() {
        return usuario;
    }

    
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasenia() {
        return contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }

    public String getRepcontrasenia() {
        return repcontrasenia;
    }

    public void setRepcontrasenia(String repcontrasenia) {
        this.repcontrasenia = repcontrasenia;
    }

    public String getSaldo() {
        return saldo;
    }

    public void setSaldo(String saldo) {
        this.saldo = saldo;
    }

    
}
