package mvc.modelo.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class UsuarioApuestaPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id_usuario")
    private int idUsuario;
    @Basic(optional = false)
    @Column(name = "id_apuesta")
    private int idApuesta;

    public UsuarioApuestaPK() {
    }

    public UsuarioApuestaPK(int idUsuario, int idApuesta) {
        this.idUsuario = idUsuario;
        this.idApuesta = idApuesta;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public int getIdApuesta() {
        return idApuesta;
    }

    public void setIdApuesta(int idApuesta) {
        this.idApuesta = idApuesta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idUsuario;
        hash += (int) idApuesta;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsuarioApuestaPK)) {
            return false;
        }
        UsuarioApuestaPK other = (UsuarioApuestaPK) object;
        if (this.idUsuario != other.idUsuario) {
            return false;
        }
        if (this.idApuesta != other.idApuesta) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mvc.modelo.entidades.UsuarioApuestaPK[ idUsuario=" + idUsuario + ", idApuesta=" + idApuesta + " ]";
    }
    
}
