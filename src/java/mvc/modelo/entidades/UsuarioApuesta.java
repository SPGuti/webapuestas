package mvc.modelo.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "usuario_apuesta")
@NamedQueries({
    @NamedQuery(name = "UsuarioApuesta.findAll", query = "SELECT u FROM UsuarioApuesta u")
    , @NamedQuery(name = "UsuarioApuesta.findByIdUsuario", query = "SELECT u FROM UsuarioApuesta u WHERE u.usuarioApuestaPK.idUsuario = :idUsuario")
    , @NamedQuery(name = "UsuarioApuesta.findByIdApuesta", query = "SELECT u FROM UsuarioApuesta u WHERE u.usuarioApuestaPK.idApuesta = :idApuesta")
    , @NamedQuery(name = "UsuarioApuesta.findByApostado", query = "SELECT u FROM UsuarioApuesta u WHERE u.apostado = :apostado")
    , @NamedQuery(name = "UsuarioApuesta.findByFinal1", query = "SELECT u FROM UsuarioApuesta u WHERE u.final1 = :final1")})
public class UsuarioApuesta implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UsuarioApuestaPK usuarioApuestaPK;
    @Basic(optional = false)
    @Column(name = "apostado")
    private double apostado;
    @Basic(optional = false)
    @Column(name = "final")
    private double final1;

    public UsuarioApuesta() {
    }

    public UsuarioApuesta(UsuarioApuestaPK usuarioApuestaPK) {
        this.usuarioApuestaPK = usuarioApuestaPK;
    }

    public UsuarioApuesta(UsuarioApuestaPK usuarioApuestaPK, double apostado, double final1) {
        this.usuarioApuestaPK = usuarioApuestaPK;
        this.apostado = apostado;
        this.final1 = final1;
    }

    public UsuarioApuesta(int idUsuario, int idApuesta) {
        this.usuarioApuestaPK = new UsuarioApuestaPK(idUsuario, idApuesta);
    }

    public UsuarioApuestaPK getUsuarioApuestaPK() {
        return usuarioApuestaPK;
    }

    public void setUsuarioApuestaPK(UsuarioApuestaPK usuarioApuestaPK) {
        this.usuarioApuestaPK = usuarioApuestaPK;
    }

    public double getApostado() {
        return apostado;
    }

    public void setApostado(double apostado) {
        this.apostado = apostado;
    }

    public double getFinal1() {
        return final1;
    }

    public void setFinal1(double final1) {
        this.final1 = final1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (usuarioApuestaPK != null ? usuarioApuestaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsuarioApuesta)) {
            return false;
        }
        UsuarioApuesta other = (UsuarioApuesta) object;
        if ((this.usuarioApuestaPK == null && other.usuarioApuestaPK != null) || (this.usuarioApuestaPK != null && !this.usuarioApuestaPK.equals(other.usuarioApuestaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mvc.modelo.entidades.UsuarioApuesta[ usuarioApuestaPK=" + usuarioApuestaPK + " ]";
    }
    
}
