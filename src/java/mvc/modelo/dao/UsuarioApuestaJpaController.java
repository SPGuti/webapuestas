package mvc.modelo.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import mvc.modelo.dao.exceptions.NonexistentEntityException;
import mvc.modelo.dao.exceptions.PreexistingEntityException;
import mvc.modelo.entidades.UsuarioApuesta;
import mvc.modelo.entidades.UsuarioApuestaPK;

public class UsuarioApuestaJpaController implements Serializable {

    public UsuarioApuestaJpaController() {
        this.emf = Persistence.createEntityManagerFactory("PU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(UsuarioApuesta usuarioApuesta) throws PreexistingEntityException, Exception {
        if (usuarioApuesta.getUsuarioApuestaPK() == null) {
            usuarioApuesta.setUsuarioApuestaPK(new UsuarioApuestaPK());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(usuarioApuesta);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findUsuarioApuesta(usuarioApuesta.getUsuarioApuestaPK()) != null) {
                throw new PreexistingEntityException("UsuarioApuesta " + usuarioApuesta + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(UsuarioApuesta usuarioApuesta) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            usuarioApuesta = em.merge(usuarioApuesta);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                UsuarioApuestaPK id = usuarioApuesta.getUsuarioApuestaPK();
                if (findUsuarioApuesta(id) == null) {
                    throw new NonexistentEntityException("The usuarioApuesta with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(UsuarioApuestaPK id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            UsuarioApuesta usuarioApuesta;
            try {
                usuarioApuesta = em.getReference(UsuarioApuesta.class, id);
                usuarioApuesta.getUsuarioApuestaPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The usuarioApuesta with id " + id + " no longer exists.", enfe);
            }
            em.remove(usuarioApuesta);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<UsuarioApuesta> findUsuarioApuestaEntities() {
        return findUsuarioApuestaEntities(true, -1, -1);
    }

    public List<UsuarioApuesta> findUsuarioApuestaEntities(int maxResults, int firstResult) {
        return findUsuarioApuestaEntities(false, maxResults, firstResult);
    }

    private List<UsuarioApuesta> findUsuarioApuestaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(UsuarioApuesta.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public UsuarioApuesta findUsuarioApuesta(UsuarioApuestaPK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(UsuarioApuesta.class, id);
        } finally {
            em.close();
        }
    }

    public int getUsuarioApuestaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<UsuarioApuesta> rt = cq.from(UsuarioApuesta.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
