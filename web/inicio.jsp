<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Página principal Cash24</title>
    </head>
    <body>
        
        <header>
            <jsp:include page="encabezado.jsp" flush="true" />
        </header>
        <nav>
            <jsp:include page="menu.jsp" flush="true" />
        </nav>
        <section>
            <jsp:include page="tablaApuestas.jsp" flush="true" />
        </section>
    </body>
</html>
