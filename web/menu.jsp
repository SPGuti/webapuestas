<%@page import="javax.persistence.Query"%>
<%@page import="javax.persistence.EntityManager"%>
<%@page import="javax.persistence.Persistence"%>
<%@page import="javax.persistence.EntityManagerFactory"%>
<%@page import="mvc.modelo.entidades.Apuesta"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<html>
    <body>
        <form method="get" action='<%=response.encodeURL("apuestas")%>'>
            <strong>Deportes</strong>
            &nbsp;
            <select name="deportes">
                <%
                    
                    EntityManagerFactory emf = Persistence.createEntityManagerFactory("PU");
                    EntityManager em = emf.createEntityManager();
                    
                    Query q = em.createNamedQuery("Apuesta.findAll");
                    List<Apuesta> deportes = q.getResultList();
                    for (Apuesta apu : deportes) {
                        out.println("<option value='" + apu.getDeporte().toLowerCase() + "'>"
                                + apu.getDeporte() + "</option>");
                    }
                %>
            </select>
            <br/><br/>
            <input type="submit" value="   CONSULTAR   " />
        </form>
    </body>
</html>
