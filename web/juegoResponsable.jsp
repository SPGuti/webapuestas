<%-- 
    Document   : sobreNosotros
    Created on : 05-abr-2018, 16:55:17
    Author     : Sandra
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="home.css">
        <title>Sobre Nosotros</title>
    </head>
   <body>
       
        <%@ include file="encabezado.jsp" %>
         
        <div  class="fondo-paginas" style=" margin-top: -150px; width: 104%; margin-left: -23px;">
            
            <div class="texto-bloque" style="font-size: 25px; color:  #ffffff;">
                <h2 class="heading"> Juego Responsable  </h2>

             <br/>
             <br/>
Web Apuestas <br/>
Protección de menores <br/><br/>
En virtud de las normas españolas sobre juegos de azar, en España la edad legal mínima para jugar es 18 años. Apuestas.com no permite la participación de jugadores menores de 18 años de edad o que hayan sido legalmente inhabilitados para administrarse a sí mismos (por medio de una sentencia judicial) en su casino o salas de poker. Esto puede apreciarse en el sitio web en todo momento.

Nuestro casino y nuestra sala de poker no están destinados a atraer niños o adolescentes. Desalentamos enérgicamente los intentos de menores de 18 años de jugar en 888.es y realizamos su seguimiento. Utilizamos sistemas de comprobación avanzados para identificar menores que se conectan a nuestro software.

También usamos software que nos permite detectar cuándo un usuario ha hecho clic en el botón para colocar una apuesta por no haber recibido una respuesta la primera vez, a fin de evitar que se realicen apuestas múltiples por error.

No obstante, somos conscientes de que Internet es un medio al que se puede acceder fácilmente desde muchos hogares en todo el mundo. Por este motivo, las compañías de juegos de azar y los padres debemos trabajar en conjunto para proteger a los menores y evitar su acceso a los juegos. Para garantizar la seguridad de tus hijos, recomendamos la instalación de un software de filtro para bloquear el acceso de menores a ciertos programas y sitios web.
            </div>
        </div>
           
    </body>
</html>
